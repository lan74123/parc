<!--footer-->
<footer class="footer text-center text-lg-left affix" data-spy="affix" data-offset-top="100">
    <div class="container footer-more">
        <a href="#Z" id="AZ" title="下方功能區塊" accesskey="Z" name="Z">:::</a>
        <div class="row">
            <div class="col-12 col-lg-4 text-center">
                <img src="{{ asset('assets/images/icon/logo-footer.svg') }}" class="logo img-fluid" alt="病人自主Logo" />
            </div>
            <div class="col-12 col-lg-4 text-center">
                <div class="footer-title">即刻行動</div>
                <a href="{{ url('/donate/story') }}" title="分享故事" class="footer-link">分享故事</a> /
                <a href="https://tlea.neticrm.tw/civicrm/contribute/transact?reset=1&id=3" target="_blank" rel="noopener noreferrer" title="我要捐款(另開視窗)" class="footer-link">我要捐款</a> /
                <a href="https://tlea.neticrm.tw/civicrm/profile/create?gid=15&reset=1" target="_blank" rel="noopener noreferrer" title="訂閱電子報(另開視窗)" class="footer-link">訂閱電子報</a>
            </div>
            <div class="col-12 col-lg-4 text-center">
                <div class="footer-title">追蹤我們</div>
                <a href="https://www.facebook.com/parc.tw/" title="facebook(另開視窗)" class="footer-social" target="_blank" rel="noopener noreferrer">
                    <img src="{{ asset('assets/images/icon/icon-facebook.svg') }}" class="img-fluid" alt="" target="_blank" rel="noopener noreferrer"/>
                </a>
                {{-- <a href="#" title="line" class="footer-social" target="_blank" rel="noopener noreferrer">
                    <img src="{{ asset('assets/images/icon/icon-line.svg') }}" class="img-fluid" alt=""/>
                </a> --}}
                <a href="https://www.youtube.com/channel/UCkJWN2WEhzH_EA5QM65GchQ" title="youtube (另開視窗)" class="footer-social" target="_blank" rel="noopener noreferrer">
                    <img src="{{ asset('assets/images/icon/icon-youtube.svg') }}" class="img-fluid" alt=""/>
                </a>
            </div>
        </div>
    </div>
    <div class="container footer-copyright">
        <a href="mailto:service@parc.tw">電子信箱 : service@parc.tw</a>
        <div>中心地址 : 10617台北市大安區羅斯福路四段1號
            <br class="d-lg-none"> 台大哲學系4F 404室</div>
        <div>病人自主研究中心 PARC © 2019. All Rights Reserved.</div>
    </div>
    <div class="container">
        <!-- GoTop -->
        <a href="#" title="至頂端" class="btn-gotop">
            <i class="fa fa-caret-up"></i>
        </a>
    </div>
</footer>
</div>